<?php

namespace App\Tests\Entity;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testSetName(){
        $user = new User();
        $user->setName("Guillaume");

        $this->assertEquals("Guillaume", $user->getName());
    }
}
