<?php

namespace App\Tests\Controller;

use App\Controller\DefaultController;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{

    public function testIndex()
    {
        $client = static::createClient();

        $client->request('GET', '/default');


        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertJson($client->getResponse()->getContent());

        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertInternalType('array', $content);
        $this->assertCount(2, $content);

        $this->assertArrayHasKey('message', $content);
        $this->assertArrayHasKey('path', $content);
    }
}
